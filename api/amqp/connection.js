var amqpConnection = null;
var confirmChannel = null;
var start = module.exports.start = function () {
	var amqp = require('amqplib/channel_api');
	var config = require('./../config.js');
	amqp.connect(config.amqp)
	.then(function (connection) {
		connection.on("error", function(error) {
			if (error.message !== "Connection closing") {
				console.error("[AMQP] conn error", error.message);
			}
		});
		connection.on("close", function() {
			console.error("[AMQP] reconnecting");
			return setTimeout(start, 1000);
		});
		console.log("[AMQP] connected");
		amqpConnection = connection;
		whenConnected();
	})
	.catch(function (error) {
		console.error("[AMQP]", error.message);
		return setTimeout(start, 1000);
	});
};
module.exports.confirmChannel = confirmChannel;

function whenConnected() {
	startPublisher();
}

function startPublisher() {
	amqpConnection.createConfirmChannel()
	.then(function(channel) {
		console.log('[AMQP] confirm channel established');
		channel.on("error", function(error) {
			console.error("[AMQP] channel error", error.message);
		});
		channel.on("close", function() {
			console.log("[AMQP] channel closed");
		});
		confirmChannel = channel;
		module.exports.confirmChannel = confirmChannel;
		var offlinePubQueue = require('./offlinePubQueue.js');
		assertContactExchanges();
		while (true) {
			var message = offlinePubQueue.shift();
			if (!message) {
				break;
			}
			publish(message[0], message[1], message[2]);
		}
	})
	.catch(function(error) {
		closeOnError();
		return;
	});
}

function assertContactExchanges() {
	confirmChannel.assertExchange('api_requests', 'contact.migrate');
}

function closeOnError(error) {
	if (!error) {
		return false;
	}
	console.error("[AMQP] error", error);
	amqpConnection.close();
	return true;
}
