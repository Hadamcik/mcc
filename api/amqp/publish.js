module.exports = function publish(exchange, routingKey, content) {
	var ampqConnection = require('./connection.js');
	var confirmChannel = ampqConnection.confirmChannel;
	var offlinePublishQueue = require('./offlinePublishQueue.js');
	try {
		confirmChannel.publish(exchange, routingKey, content, { persistent: true },
		function(error, ok) {
			if(error) {
				console.error("[AMQP] publish", error.message);
				offlinePublishQueue.push([exchange, routingKey, content]);
				confirmChannel.connection.close();
				return false;
			}
			console.log('[AMQP] published!');
			return true;
		});
	} catch (error) {
		console.error("[AMQP] publish", error.message);
		offlinePublishQueue.push([exchange, routingKey, content]);
		return false;
	}
}
