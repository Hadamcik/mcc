module.exports.amqp = {
	protocol: 'amqp',
	hostname: 'rabbitmq',
	username: 'guest',
	password: 'guest',
	port: 5672,
	locale: 'en_US',
	frameMax: 0,
	heartbeat: 60,
	vhost: '/',
}
