'use strict';
var publish = require('./../amqp/publish.js');
module.exports.migrateContact = function(req, res) {
	var contentBuffer = Buffer.from(JSON.stringify(req.swagger.params.contact.value));
	var confirm = publish('api_requests', 'contact.migrate', contentBuffer);
	res.status(confirm ? 204 : 500);
	res.end();
};
