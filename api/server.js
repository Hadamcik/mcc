var express = require('express'),
	http = require('http'),
	swaggerTools = require('swagger-tools'),
	app = express();

var serverPort = 3000;
var swaggerDoc = require('./swagger.json');
var swaggerRouterOptions = {
	controllers: './controllers',
	useStubs: true
};

var amqpConnection = require('./amqp/connection.js');
amqpConnection.start();

swaggerTools.initializeMiddleware(swaggerDoc, function (swaggerMiddleware) {
	// https://github.com/apigee-127/swagger-tools/blob/master/docs/Middleware.md
	app.use(swaggerMiddleware.swaggerMetadata());
	app.use(swaggerMiddleware.swaggerValidator({
		validateResponse: true
	}));
	app.use(swaggerMiddleware.swaggerRouter(swaggerRouterOptions));

	// Create an error handler to allow me to process errors before being sent to the client
	app.use(function(err, req, res, next) {
		if (typeof err !== 'object') {
			// If the object is not an Error, create a representation that appears to be
			err = {
				message: String(err) // Coerce to string
			};
		} else {
			// Ensure that err.message is enumerable (It is not by default)
			Object.defineProperty(err, 'message', { enumerable: true });
		}

		// Return a JSON representation of #/definitions/ErrorResponse
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(err));
	});

	http.createServer(app).listen(serverPort, function () {
		console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
	});
});
