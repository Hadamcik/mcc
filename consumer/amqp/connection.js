var amqpConnection = null;
var confirmChannel = null;
module.exports.confirmChannel = confirmChannel;
var start = module.exports.start = function () {
	var amqp = require('amqplib/channel_api');
	var config = require('./../config.js');
	amqp.connect(config.amqp)
	.then(function (connection) {
		connection.on("error", function(error) {
			if (error.message !== "Connection closing") {
				console.error("[AMQP] conn error", error.message);
			}
		});
		connection.on("close", function() {
			console.error("[AMQP] reconnecting");
			return setTimeout(start, 1000);
		});
		console.log("[AMQP] connected");
		amqpConnection = connection;
		whenConnected();
	})
	.catch(function (error) {
		console.error("[AMQP]", error.message);
		return setTimeout(start, 1000);
	});
};
var ack = module.exports.ack = function (message) {
	confirmChannel.ack(message);
};
var nack = module.exports.nack = function (message) {
	confirmChannel.nack(message);
};

function whenConnected() {
	startConsumer();
}

function startConsumer() {
	amqpConnection.createConfirmChannel()
	.then(function(channel) {
		console.log('[AMQP] confirm channel established');
		channel.on("error", function(error) {
			console.error("[AMQP] channel error", error.message);
		});
		channel.on("close", function() {
			console.log("[AMQP] channel closed");
		});
		confirmChannel = channel;
		module.exports.confirmChannel = confirmChannel;
		assertQueues();
	})
	.catch(function(error) {
		closeOnError();
		return;
	});
}

function assertQueues() {
	confirmChannel.assertQueue('contact.migrate');
	bindConsumes();
}

function bindConsumes() {
	var i = 0;
	confirmChannel.consume('contact.migrate', function(message) {
		var messageTag = message.fields.consumerTag + i++;
		var inProgress = require('./../redis/inProgress.js');
		inProgress.add(messageTag, 'contact.migrate', message)
		.then(function() {
			var Contact = require('./../controllers/Contact.js');
			var data = JSON.parse(message.content.toString());
			Contact.migrate(data)
			.then(function() {
				inProgress.remove(messageTag);
				ack(message);
			}).catch(function (error) {
				console.error('[Error] Contact migrate');
				inProgress.remove(messageTag);
				nack(message);
			});
		})
		.catch(function() {
			console.error('InProgress add failed');
		});
	}).then(function(ok) {
	}).catch(function(error) {
	});
}

function closeOnError(error) {
	if (!error) {
		return false;
	}
	console.error("[AMQP] error", error);
	amqpConnection.close();
	return true;
}
