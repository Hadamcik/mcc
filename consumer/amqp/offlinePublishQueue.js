var offlinePubQueue = [];
module.exports.push = function(exchange, routingKey, content) {
	offlinePubQueue.push([exchange, routingKey, content]);
};
module.exports.shift = function() {
	return offlinePubQueue.shift();
};
