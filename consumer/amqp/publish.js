module.exports = function publish(exchange, routingKey, content) {
	var ampqConnection = require('./connection.js');
	var confirmChannel = ampqConnection.confirmChannel;
	var offlinePublishQueue = require('./offlinePublishQueue.js');
	var Promise = require('promise');
	return new Promise(function(resolve, reject) {
		confirmChannel.publish(exchange, routingKey, content, { persistent: true })
		.then(function(ok) {
			console.log('[AMQP] published!');
			resolve();
		})
		.catch(function (error) {
			console.error("[AMQP] publish", error.message);
			offlinePublishQueue.push([exchange, routingKey, content]);
			confirmChannel.connection.close();
			reject();
		});
	});
}
