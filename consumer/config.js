module.exports.amqp = {
	protocol: 'amqp',
	hostname: 'rabbitmq',
	username: 'guest',
	password: 'guest',
	port: 5672,
	locale: 'en_US',
	frameMax: 0,
	heartbeat: 60,
	vhost: '/',
}
module.exports.redis = {
	sentinels: [
		{
			host: 'redis-sentinel-1',
			port: 26379
		},
		{
			host: 'redis-sentinel-2',
			port: 26380
		},
		{
			host: 'redis-sentinel-3',
			port: 26381
		}
	],
	name: 'mymaster',
	retryCount: 3
}
