var inProgress = require('./../redis/inProgress.js');
var Promise = require('promise');

module.exports.migrate = function(data) {
	console.log('Contact migration begin');
	return new Promise(function(resolve, reject) {
		console.log('AFTER BEGIN');
		var contactMigrate = require('./../model/contactMigrate.js');
		contactMigrate.start(data)
		.then(function () {
			console.log('[MongoDB] Contact migrated');
			resolve();
		})
		.catch(function(error) {
			console.error('[MongoDB] Contact migration failed');
			reject(error);
		});
	});
}

