var Promise = require('promise');
var db = require('./../mongodb/connection.js').db;
var publish = require('./../amqp/publish.js');
var start = module.exports.start = function (data) {
	return new Promise(function(resolve, reject) {
		prepareData(data)
		.then(function(preparedData) {
			console.log('[Success] Prepare data');
			insertContact(preparedData.contact)
			.then(function () {
				console.log('[Success] Insert contact');
				insertReferences(preparedData)
				.then(function() {
					console.log('[Success] Insert references');
					resolve(); // Everything ok, message is processed - best case scenario
				})
				.catch(function(error) {
					console.error('[InsertReferences-Error] ' + error);
					resolve(); // Contact is already in database, message has to be resolved
				});
			})
			.catch(function (error) {
				console.error('[Error] Insert contact ' + error);
				reject(); // Nothing got into database, we can reject message
			});
		})
		.catch(function(error) {
			console.error('[Error] Prepare data ' + error);
			reject(); // Nothing got into database, we can reject message
		});
	});
}

function prepareData(inputData) {
	return new Promise(function(resolve, reject) {
		var data = {};
		var devices = prepareDevicesData(inputData);
		var companies = prepareCompaniesData(inputData);
		var doNotContactRules = prepareDoNotContactRulesData(inputData);
		var frequencyRules = prepareFrequencyRulesData(inputData);
		Promise.all([devices, companies, doNotContactRules, frequencyRules])
		.then(function(values) {
			data.devices = values[0];
			data.companies = values[1];
			data.doNotContactRules = values[2];
			data.frequencyRules = values[3];
			prepareContactData(inputData, data)
			.then(function(contact) {
				data.contact = contact;
				console.log('[PrepareData-Success]');
				resolve(data);
			})
			.catch(function(error) {
				console.error('[PrepareData-Contact-Error] ' + error);
			});
		})
		.catch(function(error) {
			console.error('[PrepareData-Error] ' + error);
			reject(error);
		});
	});
}

var ObjectId = require('mongodb').ObjectId;
var instanceId = 1;
function prepareContactData(inputData, preparedData) {
	return new Promise(function(resolve, reject) {
		resolve({
			_id: new ObjectId(),
			instanceRef: {
				id: instanceId,
				refId: inputData.id
			},
			firstname: inputData.firstname,
			lastname: inputData.lastname,
			customFields: inputData.customFields,
			devices: preparedData.devices.ids,
			companies: preparedData.companies.ids,
			doNotContactRules: preparedData.doNotContactRules.ids,
			frequencyRules: preparedData.frequencyRules.ids
		});
	});
}

function prepareDevicesData(inputData) {
	var length = inputData.devices.length;
	var ids = [];
	var records = [];
	for (var i = 0; i < length; i++) {
		var migrateDevice = inputData.devices[i];
		var device = {
			_id: new ObjectId(),
			instanceRef: {
				id: instanceId,
				refId: migrateDevice.id
			},
			os: {
				name: migrateDevice.name,
				shortname: migrateDevice.shortname,
				version: migrateDevice.version,
				platform: migrateDevice.platform
			}
		};
		records.push(device);
		ids.push(device._id);
	}
	return {
		records: records,
		ids: ids
	}
}

function prepareCompaniesData(inputData) {
	var length = inputData.companies.length;
	var ids = [];
	var records = [];
	for (var i = 0; i < length; i++) {
		var migrateCompany = inputData.companies[i];
		var company = {
			_id: new ObjectId(),
			instanceRef: {
				id: instanceId,
				refId: migrateCompany.companyId
			},
			added: {
				datetime: migrateCompany.added,
				manualy: migrateCompany.manuallyAdded
			}
		};
		records.push(company);
		ids.push(company._id);
	}
	return {
		ids: ids,
		records: records
	}
}

function prepareDoNotContactRulesData(inputData) {
	var doNotContactRulesLength = inputData.doNotContactRules.length;
	var ids = [];
	var records = [];
	for (var i = 0; i < doNotContactRulesLength; i++) {
		var migrateDoNotContactRule = inputData.doNotContactRules[i];
		var doNotContactRule = {
			_id: new ObjectId(),
			instanceRef: {
				id: instanceId,
				refId: migrateDoNotContactRule.id
			},
			added: migrateDoNotContactRule.added,
			reason: migrateDoNotContactRule.reason,
			channel: {
				id: migrateDoNotContactRule.channel.id,
				name : migrateDoNotContactRule.channel.name
			}
		}
		records.push(doNotContactRule);
		ids.push(doNotContactRule._id);
	}
	return {
		ids: ids,
		records: records
	};
}

function prepareFrequencyRulesData(inputData) {
	var length = inputData.frequencyRules.length;
	var ids = [];
	var records = [];
	for (var i = 0; i < length; i++) {
		var migrateFrequencyRule = inputData.frequencyRules[i];
		var frequencyRule = {
			_id: new ObjectId(),
			instanceRef: {
				id: instanceId,
				refId: migrateFrequencyRule.id
			},
			added: migrateFrequencyRule.added,
			channel: {
				name: migrateFrequencyRule.channel.name,
				isPreferred: migrateFrequencyRule.channel.isPreferred
			},
			frequency: {
				number: migrateFrequencyRule.frequency.number,
				time: migrateFrequencyRule.frequency.time
			},
			pause: {
				from: migrateFrequencyRule.pause.from,
				to: migrateFrequencyRule.pause.to
			}
		};
		records.push(frequencyRule);
		ids.push(frequencyRule._id);
	}
	return {
		ids: ids,
		records: records
	};
}

function insertContact(contact) {
	var contactsCollection = db.collection('contacts');
	return contactsCollection.insert(contact);
}

function insertReferences(preparedData) {
	return Promise.all([
		insertDevices(preparedData.devices.records), 
		insertCompanies(preparedData.companies.records), 
		insertDoNotContactRules(preparedData.doNotContactRules.records), 
		insertFrequencyRules(preparedData.frequencyRules.records)
	]);
}

function insertDevices(devices) {
	return new Promise(function(resolve, reject) {
		if (devices.length === 0) {
			resolve();
			return;
		}
		var devicesCollection = db.collection('devices');
		devicesCollection.insertMany(devices)
		.then(function() {
			console.log('[Success] Insert devices');
			resolve();
		})
		.catch(function() {
			console.error('[Error] Insert devices');
			var contentBuffer = Buffer.from(JSON.stringify(devices));
			publish('fails', 'contact.devices.insert', contentBuffer)
			.then(function() {
				resolve(); // Failed insert logged into queue
			})
			.catch(function() {
				resolve(); // Data lost, counting on sync only
			});
		});	
	});
}

function insertCompanies(companies) {
	return new Promise(function(resolve, reject) {
		if (companies.length === 0) {
			resolve();
			return;
		}
		var companiesCollection = db.collection('companies');
		companiesCollection.insertMany(companies)
		.then(function() {
			console.log('[Success] Insert companies');
			resolve();
		})
		.catch(function() {
			console.error('[Error] Insert companies');
			var contentBuffer = Buffer.from(JSON.stringify(companies));
			publish('fails', 'contact.companies.insert', contentBuffer)
			.then(function() {
				resolve(); // Failed insert logged into queue
			})
			.catch(function() {
				resolve(); // Data lost, counting on sync only
			});
		});
	});
}

function insertDoNotContactRules(doNotContactRules) {
	return new Promise(function(resolve, reject) {
		if (doNotContactRules.length === 0) {
			resolve();
			return;
		}
		var doNotContactRulesCollection = db.collection('doNotContactRules');
		doNotContactRulesCollection.insertMany(doNotContactRules)
		.then(function() {
			console.log('[Success] Insert DoNotContactRules');
			resolve();
		})
		.catch(function() {
			console.error('[Error] Insert DoNotContactRules');
			var contentBuffer = Buffer.from(JSON.stringify(doNotContactRules));
			publish('fails', 'contact.doNotContactRules.insert', contentBuffer)
			.then(function() {
				resolve(); // Failed insert logged into queue
			})
			.catch(function() {
				resolve(); // Data lost, counting on sync only
			});
		});
	});
}

function insertFrequencyRules(frequencyRules) {
	return new Promise(function(resolve, reject) {
		if (frequencyRules.length === 0) {
			resolve();
			return;
		}
		var frequencyRulesCollection = db.collection('frequencyRules');
		frequencyRulesCollection.insertMany(frequencyRules)
		.then(function() {
			console.log('[Success] Insert FrequencyRules');
			resolve();
		})
		.catch(function() {
			console.log('[Error] Insert FrequencyRules');
			var contentBuffer = Buffer.from(JSON.stringify(frequencyRules));
			publish('fails', 'contact.frequencyRules.insert', contentBuffer)
			.then(function() {
				resolve(); // Failed insert logged into queue
			})
			.catch(function() {
				resolve(); // Data lost, counting on sync only
			});
		});
	});
}

