var db = null;
module.exports.db = db;
module.exports.start = function () {
	var MongoClient = require('mongodb').MongoClient;
	var assert = require('assert');
	MongoClient.connect('mongodb://mongodb:27017')
	.then(function(client) {
		db = client.db('mcc');
		module.exports.db = db;
		console.log("Connected successfully to server");
	})
	.catch(function(err) {
		assert.equal(null, err);
	});
}
