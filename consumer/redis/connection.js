var Redis = require('ioredis');
var config = require('./../config.js');
var redis = new Redis({
	sentinels: config.redis['sentinels'],
	name: config.redis['name'],
	connectTimeout : 1000,
	db: 0,
	sentinelRetryStrategy: (times) => {
		if (times >= config.redis['retryCount']) {
			return null;
		}
		return Math.min(((times-1) * 10) * (times * 10), 2000);
	}
});
module.exports.connection = null;
redis.connect(function(connection) {
	console.log('[Redis] Connected');
	module.exports.connection = connection;
})
.then(function(){
	console.log('[Redis sentinel] Connect succesfull');
})
.catch(function(error) {
	console.error('[Redis sentinel] Connect failed: ' + error);
});
