var redisConnection = require('./../redis/connection.js').connection;
var Promise = require('promise');

module.exports.add = function (messageTag, originalQueue, data) {
	return new Promise(function (resolve, reject) {
		var result = redisConnection.hset('inProgress', messageTag, {
			'originalQueue': originalQueue,
			'data': data
		});		
		resolve(result);
	});
};
module.exports.remove = function (messageTag) {
	return new Promise(function (resolve, reject) {
		var result = redisConnection.hdel('inProgress', messageTag);
		resolve(result);
	});
};
