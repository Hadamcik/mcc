var express = require('express'),
	app = express();

var mongodbConnection = require('./mongodb/connection.js');
mongodbConnection.start();

var amqpConnection = require('./amqp/connection.js');
amqpConnection.start();
